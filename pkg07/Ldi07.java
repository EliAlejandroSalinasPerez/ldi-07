/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ldi.pkg07;

/**
 *
 * @author Eli
 */
public class Ldi07 {

  public String ComplementoadosHex(String hexadecimal){
      int h1=HexadecimalaDecimal(hexadecimal);
      String arestar="";
     // int tamhexa=hexadecimal.length();
      
      while(arestar.length()<hexadecimal.length()){
          arestar+="F";
      }
      
      
      int h2=HexadecimalaDecimal(arestar);
      
      int resta=h2-h1;
      int total=resta+1;
      
      return DecimalaHexadecimal(total).toUpperCase();
  }
    
  private int HexadecimalaDecimal(String cad){
      return Integer.parseInt(cad,16);
  }
    
  private String DecimalaHexadecimal(int i){
      return Integer.toHexString(i).toUpperCase();
  }  
    
  public String hexSignoaDec(String Hexadecimal){
     // int valorhexadecimal=HexadecimalaDecimal(Hexadecimal);
     String hexaMayus=Hexadecimal.toUpperCase();
      char[] arre=hexaMayus.toCharArray();
      
      if(arre[0]=='7'|| arre[0]=='6'|| arre[0]=='5'|| arre[0]=='4'|| arre[0]=='3'|| arre[0]=='2'|| arre[0]=='1'|| arre[0]=='0'){
          //positivos
          return "+"+HexadecimalaDecimal(Hexadecimal);
      }
      if(arre[0]=='8'||arre[0]=='9'||arre[0]=='A'||arre[0]=='B'||arre[0]=='C'||arre[0]=='D'||arre[0]=='E'||arre[0]=='F'){
          //negativos
        return"-" +HexadecimalaDecimal(ComplementoadosHex(Hexadecimal));
        
      }
      
      return "Error";
  }
  public String decimalSinaHex(int decimal){
      int valorabsoluto=Math.abs(decimal);
      String valorabsolutoHex=DecimalaHexadecimal(valorabsoluto);
      
      if(decimal>0){
          return valorabsolutoHex;
      }
      if(decimal<0){
          return ComplementoadosHex(valorabsolutoHex);
      }
      
      return "0";
  }
  
    public static void main(String[] args) {
        Ldi07 l7=new Ldi07();
        System.out.println("Complemento a Dos:" +
        l7.ComplementoadosHex("21F0"));
        System.out.println("Connversion HEX a DEC: "+
          l7.hexSignoaDec("8A20")  );
        
        System.out.println("Conversion de DEC a Hex: "+ l7.decimalSinaHex(15));
    }
    
}
